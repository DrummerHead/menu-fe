import React from 'react';

import './css/menu-list.css';
import './css/section.css';


class MenuList extends React.Component {
  restructureData(data) {
    const newStructure = new Map();
    for (const category of data.categories) {
      const itemsThatBelongToThisCategory = data.items.filter((item) =>
        item.category_id === category.category_id
      );
      newStructure.set(category.title, itemsThatBelongToThisCategory);
    }
    return newStructure;
  }

  render() {
    const menuList = this.props.menu.items
      ? (Array.from(this.restructureData(this.props.menu)).map(([category, items]) =>
        <section className='section' key={category} id={category}>
          <h2 className='section__title'>{category}</h2>
          <ul className='menu-list'>
            {items.map(item =>
              <li className='menu-list__item' key={item.id}>
                <header className='menu-list__header'>
                  <strong className='menu-list__title'><span>{item.title}</span></strong>
                  <span className='menu-list__price'>{item.price}</span>
                </header>
                <span className='menu-list__description'>{item.description}</span>
              </li>
            )}
          </ul>
        </section>
      ))
      : <span className='loading'>Loading menu</span>
      ;

    return (
      <div className='vessel'>
        {menuList}
      </div>
    );
  }
}

export default MenuList;