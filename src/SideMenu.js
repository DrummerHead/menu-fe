import React from 'react';

import './css/side-menu.css'

class SideMenu extends React.Component {
  constructor() {
    super();
    this.getDivWidth = this.getDivWidth.bind(this);
  }

  getDivWidth(div) {
    this.props.getSideMenuWidth(div.offsetWidth);
  }

  render() {
    const categoryList = this.props.categories
      ? this.props.categories.map(category =>
          <li key={category.category_id}>{category.title}</li>)
      : <li>Loading</li>;

    return (
      <div className={`side-menu ${this.props.isOpen ? 'side-menu--open' : 'side-menu--closed'}`} ref={this.getDivWidth}>
        <h2 className='side-menu__title'>Categories</h2>
        <ul>
          {categoryList}
        </ul>
      </div>
    );
  }
}

export default SideMenu
