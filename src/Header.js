import React from 'react';
import './css/header.css';
import './css/branding.css';
import './css/hamburger.css'

class Header extends React.Component {
  render() {
    return (
      <header className='app-header'>
        <span className='hamburger app-header__item' onClick={this.props.toggleSideMenu}>{this.props.isSideMenuOpen ? '×' : '☰'}</span>
        <span className='branding branding--text app-header__item'>Restaurant</span>
      </header>);
  }
}

export default Header;
