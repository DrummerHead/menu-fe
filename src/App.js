import React from 'react';
import axios from 'axios';

import MenuList from './MenuList'
import Header from './Header'
import SideMenu from './SideMenu'

import './css/baseline.css';
import './css/paper.css'

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      isSideMenuOpen: false,
      sideMenuWidth: 100,
      menuData: {
        items: null,
        categories: null,
      },
    };

    this.toggleSideMenu = this.toggleSideMenu.bind(this);
    this.toggleMainStyle = this.toggleMainStyle.bind(this);
    this.getSideMenuWidth = this.getSideMenuWidth.bind(this);
  }

  componentDidMount() {
    axios.get('/v1/')
      .then((res) => {
        this.setState({
          menuData: res.data
        });
      })
      .catch(error =>
        console.log(error)
      );
  }

  toggleSideMenu() {
    this.setState(prevState => ({
      isSideMenuOpen: !prevState.isSideMenuOpen
    }));
  }

  getSideMenuWidth(width) {
    this.setState({
      sideMenuWidth: width
    });
  }

  toggleMainStyle() {
    return {transform: `translateX(${this.state.isSideMenuOpen ? this.state.sideMenuWidth : 0}px)`};
  }

  render() {
    return (
      <div className={`paper ${this.state.isSideMenuOpen ? 'paper--sideMenuOpen' : 'paper--sideMenuClosed'}`}>
        <SideMenu categories={this.state.menuData.categories} isOpen={this.state.isSideMenuOpen} getSideMenuWidth={this.getSideMenuWidth} />
        <main className='paper__main' style={this.toggleMainStyle()}>
          <Header toggleSideMenu={this.toggleSideMenu} isSideMenuOpen={this.state.isSideMenuOpen}/>
          <MenuList menu={this.state.menuData}/>
        </main>
      </div>
    );
  }
}

export default App;
